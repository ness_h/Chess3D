#include <SDL/SDL.h>
#include <windows.h>
#include <GL/gl.h>
#include <iostream>
#include <GL/glu.h>
#include "sdlglutils.h"
#include <cstdlib>
#include <vector>
#include "OBJlib.h"
#include "3dpart.h"
#define W 800
#define H 600
#define FPS 30

using namespace std;


TrackBallCamera::TrackBallCamera()
    {
    const char *hand1[] =
        {
            /* width height num_colors chars_per_pixel */
            " 16 16 3 1 ",
            /* colors */
            "X c #000000",
            ". c #ffffff",
            "  c None",
            /* pixels */
            "       XX       ",
            "   XX X..XXX    ",
            "  X..XX..X..X   ",
            "  X..XX..X..X X ",
            "   X..X..X..XX.X",
            "   X..X..X..X..X",
            " XX X.......X..X",
            "X..XX..........X",
            "X...X.........X ",
            " X............X ",
            "  X...........X ",
            "  X..........X  ",
            "   X.........X  ",
            "    X.......X   ",
            "     X......X   ",
            "     X......X   ",
            "0,0"
        };

    const char *hand2[] =
        {
            /* width height num_colors chars_per_pixel */
            " 16 16 3 1 ",
            /* colors */
            "X c #000000",
            ". c #ffffff",
            "  c None",
            /* pixels */
            "                ",
            "                ",
            "                ",
            "                ",
            "    XX XX XX    ",
            "   X..X..X..XX  ",
            "   X........X.X ",
            "    X.........X ",
            "   XX.........X ",
            "  X...........X ",
            "  X...........X ",
            "  X..........X  ",
            "   X.........X  ",
            "    X.......X   ",
            "     X......X   ",
            "     X......X   ",
            "0,0"
        };
    _hand1 = cursorFromXPM(hand1); //création du curseur normal
    _hand2 = cursorFromXPM(hand2); //création du curseur utilisé quand le bouton est enfoncé
    SDL_SetCursor(_hand1); //activation du curseur normal
    _hold = false; //au départ on part du principe que le bouton n'est pas maintenu
    _angleZ =30;
    _angleY = 1;
    _distance = 2.5; //distance initiale de la caméra avec le centre de la scène
    _motionSensivity = 0.3;
    _scrollSensivity = 0.2;
}



     void TrackBallCamera::OnMouseMotion(const SDL_MouseMotionEvent & event)

{

    if (_hold) //si nous maintenons le bouton gauche enfoncé

    {

        _angleZ += event.yrel*_motionSensivity; //mouvement sur X de la souris -> changement de la rotation horizontale

        _angleY += event.xrel*_motionSensivity; //mouvement sur Y de la souris -> changement de la rotation verticale

        //pour éviter certains problèmes, on limite la rotation verticale à des angles entre -90° et 90°

    }

}

    void TrackBallCamera::OnMouseButton(const SDL_MouseButtonEvent & event)
    {
    if (event.button == SDL_BUTTON_LEFT) //l'événement concerne le bouton gauche
    {
        if ((_hold)&&(event.type == SDL_MOUSEBUTTONUP)) //relâchement alors qu'on était enfoncé
        {
            _hold = false; //le mouvement de la souris ne fera plus bouger la scène
            SDL_SetCursor(_hand1); //on met le curseur normal
        }
        else if ((!_hold)&&(event.type == SDL_MOUSEBUTTONDOWN)) //appui alors qu'on était relâché
        {
            _hold = true; //le mouvement de la souris fera bouger la scène
            SDL_SetCursor(_hand2); //on met le curseur spécial
        }
    }
    else if ((event.button == SDL_BUTTON_WHEELUP)&&(event.type == SDL_MOUSEBUTTONDOWN)) //coup de molette vers le haut
    {
        _distance -= _scrollSensivity; //on zoome, donc rapproche la caméra du centre
        if (_distance < 0.1) //distance minimale, à changer si besoin (avec un attribut par exemple)
            _distance = 0.1;
    }
    else if ((event.button == SDL_BUTTON_WHEELDOWN)&&(event.type == SDL_MOUSEBUTTONDOWN)) //coup de molette vers le bas
    {
            _distance += _scrollSensivity; //on dézoome donc éloigne la caméra
    }
}

     void TrackBallCamera::OnKeyboard(const SDL_KeyboardEvent & event)

{

    if ((event.type == SDL_KEYDOWN)&&(event.keysym.sym == SDLK_SPACE)) //appui sur la touche HOME

    {

        _angleZ = 0; //remise à zéro des angles

        _angleY = 0;

    }

}


    void TrackBallCamera::look()
    {

    gluLookAt(_distance,0,0,

              0,0,0,

              0,0,1); // la caméra regarde le centre (0,0,0) et est sur l'axe X à une certaine distance du centre donc (_distance,0,0)
 glRotated(-90,-15,0,1);
 glRotated(-_angleZ,0,0,1); //la scène est tournée autour de l'axe Y

    glRotated(_angleY,0,1,0); //la scène est tournée autour de l'axe Z


}

//    void TrackBallCamera::setMotionSensivity(double sensivity){}

//     void TrackBallCamera::setScrollSensivity(double sensivity){}


    TrackBallCamera::~TrackBallCamera()
    {

    SDL_FreeCursor(_hand1); //destruction du curseur normal

    SDL_FreeCursor(_hand2); //destruction du curseur spécial

    SDL_SetCursor(NULL); //on remet le curseur par défaut.

}

 bool getmn (Uint16 x,Uint16 y,int* m,int* n)
 {

     GLint c[4];
                GLfloat Z;
                GLdouble a[16],b[16],CO[3];
                glGetDoublev(GL_PROJECTION_MATRIX,b);
                glGetDoublev(GL_MODELVIEW_MATRIX,a);
                glGetIntegerv(GL_VIEWPORT,c);
                glReadPixels(x, H-y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &Z);

            gluUnProject(x,H-y,Z,a,b,c,CO,CO+1,CO+2);
for (int i=0;i<3;i++)
    CO[i]*=100;
if (CO[1]>23&&CO[1]<130&&CO[0]>-100&&CO[0]<100&&CO[2]>-100&&CO[2]<100)
{

    *n=-(CO[0]-100)/25;
    *m=-(CO[2]-100)/25;
    return true;

}
else return false;
 }


