#include "piece.h"
#include <typeinfo>
piece::piece (short m,short n,bool c,MeshObj* i):col(c){
    p.x=m;p.y=n;img=i;s=false;};
void piece::add (short m,short n)
{p.x+=m;p.y+=n;}

short piece::getx(){return p.x;}
short piece::gety(){return p.y;}
void piece::setxy (short m,short n)
{p.x=m;p.y=n;}




void piece::select (piece* M[8][8])
{
    pos p1;
    if (s)
    {s=false;
   allowedxy(false,M);

    block(&allowed);
    }
    else
    {   s=true;

        allowedxy(false,M);

        block(&allowed);
    }
}
void  piece::moveto (int m,short n,piece* M [8] [8])
{   short a ;
    if (flag.x==m&&flag.y==n)
    M[p.y][m]=NULL;

    M[p.y][p.x]=NULL;a=p.y-n;p.x=m;p.y=n;M[n][m]=this;if (a==2||a==-2){flag.x=m;flag.y=n+a/2;}

}


void piece::allowedxy (bool free,piece* M [8] [8])
{ int x,y;
bool test=true;
blocked=false;
    if (col)

{


    x=p.x-Wking->p.x;
     y=p.y-Wking->p.y;
if (abs(x)==abs(y)||x==0||y==0)
{
if (x!=0)x/=abs(x);
if (y!=0)y/=abs(y);

for (int i=1;i<7&&((Wking->p.x+i*x)>=0&&(Wking->p.x+i*x)<8&&(Wking->p.y+i*y)>=0&&(Wking->p.y+i*y)<8&&test);i++)
{
    if (M[Wking->p.y+i*y][Wking->p.x+i*x]!=NULL)
    {if (M[Wking->p.y+i*y][Wking->p.x+i*x]->col!=col)
    {
        test=false;
        if (typeid(*(M[Wking->p.y+i*y][Wking->p.x+i*x])).name()==typeid(queen).name()||typeid(*(M[Wking->p.y+i*y][Wking->p.x+i*x])).name()==typeid(rook).name()||typeid(*(M[Wking->p.y+i*y][Wking->p.x+i*x])).name()==typeid(bishop).name())
            {blocked=true;
            if ((typeid(*this).name()==typeid(queen).name()||typeid(*this).name()==typeid(rook).name())&&((abs(x)==1&&y==0)||(x==0&&abs(y)==1)))
                {pos p1;
                    p1.x=Wking->p.x+i*x;
                    p1.y=Wking->p.y+i*y;
                    allowed.push_back (p1);
                }
                if ((typeid(*this).name()==typeid(queen).name()||typeid(*this).name()==typeid(bishop).name())&&(abs(x)==1&&abs(y)==1))
                {pos p1;
                    p1.x=Wking->p.x+i*x;
                    p1.y=Wking->p.y+i*y;
                     allowed.push_back (p1);
                }
                if (typeid(*this).name()==typeid(pion).name()&&((p.x+1==Wking->p.x+i*x||p.x-1==Wking->p.x+i*x)&&p.y+1==Wking->p.y+i*y))
                {pos p1;

                    p1.x=Wking->p.x+i*x;
                    p1.y=Wking->p.y+i*y;
                     allowed.push_back (p1);
                }
            }
    }
    else if (M[Wking->p.y+i*y][Wking->p.x+i*x]!=this)
    {
        test=false;
    }

    }

}
}
}
else


    {

x=p.x-Bking->p.x;
     y=p.y-Bking->p.y;
if (abs(x)==abs(y)||x==0||y==0)
{if (x!=0)x/=abs(x);
if (y!=0)y/=abs(y);

for (int i=1;i<7&&((Bking->p.x+i*x)>=0&&(Bking->p.x+i*x)<8&&(Bking->p.y+i*y)>=0&&(Bking->p.y+i*y)<8&&test);i++)
{
    if (M[Bking->p.y+i*y][Bking->p.x+i*x]!=NULL)
    {if (M[Bking->p.y+i*y][Bking->p.x+i*x]->col!=col)
    {
        test=false;
        if (typeid(*(M[Bking->p.y+i*y][Bking->p.x+i*x])).name()==typeid(queen).name()||typeid(*(M[Bking->p.y+i*y][Bking->p.x+i*x])).name()==typeid(rook).name()||typeid(*(M[Bking->p.y+i*y][Bking->p.x+i*x])).name()==typeid(bishop).name())
            {blocked=true;
            if ((typeid(*this).name()==typeid(queen).name()||typeid(*this).name()==typeid(rook).name())&&((abs(x)==1&&y==0)||(x==0&&abs(y)==1)))
                {pos p1;
                    p1.x=Bking->p.x+i*x;
                    p1.y=Bking->p.y+i*y;
                    allowed.push_back (p1);
                }
                if ((typeid(*this).name()==typeid(queen).name()||typeid(*this).name()==typeid(bishop).name())&&(abs(x)==1&&abs(y)==1))
                {pos p1;
                    p1.x=Bking->p.x+i*x;
                    p1.y=Bking->p.y+i*y;
                     allowed.push_back (p1);
                }
                if (typeid(*this).name()==typeid(pion).name()&&((p.x+1==Bking->p.x+i*x||p.x-1==Bking->p.x+i*x)&&p.y-1==Bking->p.y+i*y))
                {pos p1;

                    p1.x=Bking->p.x+i*x;
                    p1.y=Bking->p.y+i*y;
                     allowed.push_back (p1);
                }
            }

    }
    else if (M[Bking->p.y+i*y][Bking->p.x+i*x]!=this)
    {
        test=false;
    }

    }

}

}
}
}
bool piece::movexy(short m,short n,piece* M [8] [8])
{


bool b=false ;

if(allowed.empty()==false)
{for (vector<pos>::iterator i=allowed.begin();i<allowed.end();++i )

if (m==i->x&&n==i->y)
b=true;
}

if (!b)
{return false ;}
else
{moveto(m,n,M);
if (col&&((king*)Wking)->check.size()==1)
{
    ((king*)Wking)->check.clear();
   // blocked=true;

((king*)Wking)->check[0]->allowedxy(false,M);
}
else if (!col&&((king*)Bking)->check.size()==1)
{
    ((king*)Bking)->check.clear();
    //blocked=true;

    ((king*)Bking)->check[0]->allowedxy(false,M);

}

}


return (true);

}



void piece::draw(piece* M[8][8]){

glPushMatrix();

glTranslated(0.12+0.75-p.y*0.25,0.25,0.08+0.75-p.x*0.25);
glScaled(0.5,0.5,0.5);
if (img!=NULL)
img->draw_model();
glPopMatrix();
if (s)
{ glDisable(GL_LIGHTING);
  for (vector<pos>::iterator it=allowed.begin();it<allowed.end();++it )
  {
      glBegin(GL_QUADS);
      if (M[it->y][it->x]==NULL)
    glColor4ub(150,230,150,100);
    else
    {if (M[it->y][it->x]->col!=col)glColor4ub(230,150,150,160);if (M[it->y][it->x]->col==col)glColor4ub(180,150,230,160);}

    glVertex3d(1.0-(it->y*0.25),0.2501,1-(it->x*0.25));
    glVertex3d(1.0-(it->y*0.25+0.25),0.2501,1-(it->x*0.25));
    glVertex3d(1.0-(it->y*0.25+0.25),0.2501,1-(it->x*0.25+0.25));
    glVertex3d(1.0-(it->y*0.25),0.2501,1-(it->x*0.25+0.25));glEnd();
  }


    glBegin(GL_QUADS);
    glColor4ub(230,230,150,120);

    glVertex3d(1.0-(p.y*0.25),0.2501,1-(p.x*0.25));
    glVertex3d(1.0-(p.y*0.25+0.25),0.2501,1-(p.x*0.25));
    glVertex3d(1.0-(p.y*0.25+0.25),0.2501,1-(p.x*0.25+0.25));
    glVertex3d(1.0-(p.y*0.25),0.2501,1-(p.x*0.25+0.25));glEnd();
glEnable(GL_LIGHTING);
   }


}



         king::king (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){if (col)piece::img=new MeshObj("Chess/kb.obj");else piece::img=new MeshObj("Chess/kn.obj");castling.x=1;castling.y=1;};

void king::prohibitedxy (piece* M [8] [8])
{
pos p1;

if (!prohibited.empty())
prohibited.clear();
for (short i=0;i<8;i++)
   for (short j=0;j<8;j++)
{
    if (M[i][j]!=NULL&&M[i][j]->col!=col)
    {
        if (typeid((*M[i][j])).name()==typeid(pion).name())
        {
            if (col&&M[i][j]->p.y-2==p.y)
            {

                if (M[i][j]->p.x==p.x+2)
                {
                    p1.x=p.x+1;
                    p1.y=p.y+1;
                    prohibited.push_back(p1);
                }
                if (M[i][j]->p.x==p.x-2)
                {
                    p1.x=p.x-1;
                    p1.y=p.y+1;
                    prohibited.push_back(p1);
                }
                if (M[i][j]->p.x==p.x+1)
                {
                    p1.x=p.x;
                    p1.y=p.y+1;
                    prohibited.push_back(p1);
                }
                if (M[i][j]->p.x==p.x-1)
                {
                    p1.x=p.x;
                    p1.y=p.y+1;
                    prohibited.push_back(p1);
                }
            }
            if (!col&&M[i][j]->p.y+2==p.y)
            {

                if (M[i][j]->p.x==p.x+2)
                { 
                    p1.x=p.x+1;
                    p1.y=p.y-1;
                    prohibited.push_back(p1);
                }
                if (M[i][j]->p.x==p.x-2)
                { 
                    p1.x=p.x-1;
                    p1.y=p.y-1;
                    prohibited.push_back(p1);
                }
                 if (M[i][j]->p.x==p.x+1)
                { 
                    p1.x=p.x;
                    p1.y=p.y-1;
                    prohibited.push_back(p1);
                }
                if (M[i][j]->p.x==p.x-1)
                { 
                    p1.x=p.x;
                    p1.y=p.y-1;
                    prohibited.push_back(p1);
                }
            }
        }
        for (vector<pos>::iterator it=M[i][j]->allowed.begin();it<M[i][j]->allowed.end();it++ )
        {
            if (it->y==p.y+1&&it->x==p.x)
            {
                p1.x=p.x;p1.y=p.y+1; prohibited.push_back(p1);}
                if (it->y==p.y&&it->x==p.x+1)
            {
                p1.x=p.x+1;p1.y=p.y; prohibited.push_back(p1);}
                if (it->y==p.y&&it->x==p.x-1)
            {p1.x=p.x-1;p1.y=p.y; prohibited.push_back(p1);}
                if (it->y==p.y-1&&it->x==p.x)
            {
                p1.x=p.x;p1.y=p.y-1; prohibited.push_back(p1);}
                if (it->y==p.y+1&&it->x==p.x+1)
            {
                p1.x=p.x+1;p1.y=p.y+1; prohibited.push_back(p1);}
                if (it->y==p.y-1&&it->x==p.x-1)
            {
                p1.x=p.x-1;p1.y=p.y-1; prohibited.push_back(p1);}
                if (it->y==p.y+1&&it->x==p.x-1)
            {
                p1.x=p.x-1;p1.y=p.y+1; prohibited.push_back(p1);}
                if (it->y==p.y-1&&it->x==p.x+1)
            {
                p1.x=p.x+1;p1.y=p.y-1; prohibited.push_back(p1);}
                if (it->y==p.y&&it->x==p.x)
            {
                check.push_back(M[i][j]);
                cout <<"check"<<check.size()<<endl;
                }

        }
    }
}
}

void king::allowedxy(bool free,piece* M [8] [8])

{allowed.clear();
pos p1;

   {
       if(p.y+1<8)
    {
        if((M[p.y+1][p.x]==NULL||M[p.y+1][p.x]->col!=col))
        {p1.y=p.y+1;p1.x=p.x;allowed.push_back(p1);}

    }

    if(p.x+1<8)
    {
        if(M[p.y][p.x+1]==NULL)
        {p1.y=p.y;p1.x=p.x+1;allowed.push_back(p1);}
        else
        {
            if (M[p.y][p.x+1]->col!=col)
                {p1.y=p.y;p1.x=p.x+1;allowed.push_back(p1);}


        }
    }
      if(p.x-1>=0)
    {
        if(M[p.y][p.x-1]==NULL)
        {p1.y=p.y;p1.x=p.x-1;allowed.push_back(p1);}
        else
        {
            if (M[p.y][p.x-1]->col!=col)
                {p1.y=p.y;p1.x=p.x-1;allowed.push_back(p1);}


        }
    }
     if(p.y-1>=0)
    {
        if(M[p.y-1][p.x]==NULL)
        {p1.y=p.y-1;p1.x=p.x;allowed.push_back(p1);}
        else
        {
            if (M[p.y-1][p.x]->col!=col)
                {p1.y=p.y-1;p1.x=p.x;allowed.push_back(p1);}


        }
    }
     if(p.y+1<8&&p.x+1<8)
    {
        if(M[p.y+1][p.x+1]==NULL)
        {p1.y=p.y+1;p1.x=p.x+1;allowed.push_back(p1);}
        else
        {
            if (M[p.y+1][p.x+1]->col!=col)
                {p1.y=p.y+1;p1.x=p.x+1;allowed.push_back(p1);}


        }
    }
    if(p.y-1>=0&&p.x-1>=0)
    {
        if(M[p.y-1][p.x-1]==NULL)
        {p1.y=p.y-1;p1.x=p.x-1;allowed.push_back(p1);}
        else
        {
            if (M[p.y-1][p.x-1]->col!=col)
                {p1.y=p.y-1;p1.x=p.x-1;allowed.push_back(p1);}

        }
    }
      if(p.y+1<8&&p.x-1>=0)
    {
        if(M[p.y+1][p.x-1]==NULL)
        {p1.y=p.y+1;p1.x=p.x-1;allowed.push_back(p1);}
        else
        {
            if (M[p.y+1][p.x-1]->col!=col)
                {p1.y=p.y+1;p1.x=p.x-1;allowed.push_back(p1);}


        }
    }
      if(p.y-1>=0&&p.x+1<8)
    {
        if(M[p.y-1][p.x+1]==NULL)
        {p1.y=p.y-1;p1.x=p.x+1;allowed.push_back(p1);}
        else
        {
            if (M[p.y-1][p.x+1]->col!=col)
               {p1.y=p.y-1;p1.x=p.x+1;allowed.push_back(p1);}


        }
    }
   }
   if (castling.x!=0&&M[p.y][2]==NULL&&M[p.y][1]==NULL&&M[p.y][3]==NULL)
   {p1.x=2;p1.y=p.y;allowed.push_back(p1);}
   if (castling.x!=0&&M[p.y][6]==NULL&&M[p.y][5]==NULL)
   {p1.x=6;p1.y=p.y;allowed.push_back(p1);}
check.clear();
   prohibitedxy(M);


   if (!prohibited.empty())
   {
short a=prohibited.size();
for (int i=0;i<allowed.size();i++)
{ bool x =true;
    for (int j=0;j<a;j++)
    if (x==true&&allowed[i].x==prohibited[j].x&&allowed[i].y==prohibited[j].y)
    {
       
        allowed.erase(allowed.begin()+i);
        x=false;
    }
}
   }


;}
bool king::movexy (short  m,short n,piece* M[8][8])
{
    if (piece::movexy (m,n,M))
    {
        if (castling.x!=0||castling.y!=0)
            {
                if (m==2&&n==p.y&&castling.x!=0)
                {
                    M[p.y][0]->moveto(3,p.y,M);
                }
                if (m==6&&n==p.y&&castling.y!=0)
                {
                    M[p.y][7]->moveto(5,p.y,M);
                }
                castling.x=0;castling.y=0;
            }

        return true;}
    return false;

}



pos piece::block(vector <pos>* V){
pos p1;
p1.x=9;p1.y=9;


if (typeid(*this).name()!=typeid(king).name())

{if (col)
    {if (Wking->check.size()>0)
{
    if (Wking->check.size()>1)
        {V->clear();}
    else
        {

        piece* pi = (Wking)->check[0];

        for (int j=0;j<V->size();j++)
        {
            if (!checkdir(Wking->p,(*V)[j],pi->p))
            {

              V->erase(V->begin()+j);
              j--;}

}


        }
}}
else
   {
    if ((Bking)->check.size()>0)
{

    if ((Bking)->check.size()>1)
        V->clear();
    else
        {
piece* pi = (Bking)->check[0];
        for (int j=0;j<V->size();j++)
        {
            if (!checkdir(Bking->p,(*V)[j],pi->p))
            {
 
              V->erase(V->begin()+j);
              j--;}}

}

}}}
return p1;}



     queen::queen (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){if (col)piece::img=new MeshObj("Chess/qb.obj");else piece::img=new MeshObj("Chess/qn.obj");};

    void queen::allowedxy(bool free,piece* M [8] [8])
    {   allowed.clear();
        pos p1;
       bool T[8]={false,false,false,false,false,false,false,false};

piece::allowedxy(free,M);

 if (!blocked)
 {

for (int i=1;i<8&& (p.y+i<8||p.y-i>=0||p.x+i<8||p.x-i>=0);i++)

   {
       if(!T[0]&&p.y+i<8)
    {
        if(M[p.y+i][p.x]==NULL||(free&&M[p.y+i][p.x]->col!=col))
        {p1.y=p.y+i;p1.x=p.x;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+i][p.x]->col!=col)
               {p1.y=p.y+i;p1.x=p.x;allowed.push_back(p1);}
            T[0]=true;

        }
    }

    if(!T[1]&&p.x+i<8)
    {
        if(M[p.y][p.x+i]==NULL||(free&&M[p.y+i][p.x]->col!=col))
        {p1.y=p.y;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y][p.x+i]->col!=col)
                {p1.y=p.y;p1.x=p.x+i;allowed.push_back(p1);}
            T[1]=true;

        }
    }
      if(!T[2]&&p.x-i>=0)
    {
        if(M[p.y][p.x-i]==NULL||(free&&M[p.y][p.x-1]->col!=col))
        {p1.y=p.y;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y][p.x-i]->col!=col)
                {p1.y=p.y;p1.x=p.x-i;allowed.push_back(p1);}
            T[2]=true;

        }
    }
     if(!T[3]&&p.y-i>=0)
    {
        if(M[p.y-i][p.x]==NULL||(free&&M[p.y-i][p.x]->col!=col))
        {p1.y=p.y-i;p1.x=p.x;allowed.push_back(p1);}
        else
        {
          //  if (M[p.y-i][p.x]->col!=col)
                {p1.y=p.y-i;p1.x=p.x;allowed.push_back(p1);}
            T[3]=true;

        }
    }
     if(!T[4]&&p.y+i<8&&p.x+i<8)
    {
        if(M[p.y+i][p.x+i]==NULL||(free&&M[p.y+i][p.x+i]->col!=col))
        {p1.y=p.y+i;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+i][p.x+i]->col!=col)
                {p1.y=p.y+i;p1.x=p.x+i;allowed.push_back(p1);}
            T[4]=true;

        }
    }
    if(!T[5]&&p.y-i>=0&&p.x-i>=0)
    {
        if(M[p.y-i][p.x-i]==NULL||(free&&M[p.y-i][p.x+i]->col!=col))
        {p1.y=p.y-i;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-i][p.x-i]->col!=col)
                {p1.y=p.y-i;p1.x=p.x-i;allowed.push_back(p1);}
            T[5]=true;

        }
    }
      if(!T[6]&&p.y+i<8&&p.x-i>=0)
    {
        if(M[p.y+i][p.x-i]==NULL||(free&&M[p.y+i][p.x-i]->col!=col))
        {p1.y=p.y+i;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+i][p.x-i]->col!=col)
                {p1.y=p.y+i;p1.x=p.x-i;allowed.push_back(p1);}
            T[6]=true;

        }
    }
      if(!T[7]&&p.y-i>=0&&p.x+i<8)
    {
        if(M[p.y-i][p.x+i]==NULL||(free&&M[p.y-i][p.x+i]->col!=col))
        {p1.y=p.y-i;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-i][p.x+i]->col!=col)
               {p1.y=p.y-i;p1.x=p.x+i;allowed.push_back(p1);}
            T[7]=true;

        }
    }
   }

 }

;}



         rook::rook (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){if (col)piece::img=new MeshObj("Chess/rb.obj");else piece::img=new MeshObj("Chess/rn.obj");};
void rook::allowedxy(bool free,piece* M [8] [8])
    {   allowed.clear();
        pos p1;
       bool T[4]={false,false,false,false};



piece::allowedxy(free,M);

 if (!blocked)
 {


for (int i=1;i<8&& (p.y+i<8||p.y-i>=0||p.x+i<8||p.x-i>=0);i++)

   {
       if(!T[0]&&p.y+i<8)
    {
        if(M[p.y+i][p.x]==NULL||(free&&M[p.y+i][p.x]->col!=col))
        {p1.y=p.y+i;p1.x=p.x;allowed.push_back(p1);}
        else
        {
           // if (M[p.y+i][p.x]->col!=col)
                {p1.y=p.y+i;p1.x=p.x;allowed.push_back(p1);}
            T[0]=true;

        }
    }

    if(!T[1]&&p.x+i<8)
    {
        if(M[p.y][p.x+i]==NULL||(free&&M[p.y][p.x+i]->col!=col))
        {p1.y=p.y;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y][p.x+i]->col!=col)
                {p1.y=p.y;p1.x=p.x+i;allowed.push_back(p1);}
            T[1]=true;

        }
    }
      if(!T[2]&&p.x-i>=0)
    {
        if(M[p.y][p.x-i]==NULL||(free&&M[p.y][p.x+i]->col!=col))
        {p1.y=p.y;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y][p.x-i]->col!=col)
                {p1.y=p.y;p1.x=p.x-i;allowed.push_back(p1);}
            T[2]=true;

        }
    }
     if(!T[3]&&p.y-i>=0)
    {
        if(M[p.y-i][p.x]==NULL||(free&&M[p.y-i][p.x]->col!=col))
        {p1.y=p.y-i;p1.x=p.x;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-i][p.x]->col!=col)
                {p1.y=p.y-i;p1.x=p.x;allowed.push_back(p1);}
            T[3]=true;

        }
    }


   }
 }
;}
bool rook::movexy (short m,short n,piece* M[8][8])
{ int y=p.y;
    if (piece::movexy (m,n,M))
    {if (p.x==0&&((king*)M[y][4])->castling.x!=0)
    ((king*)M[y][4])->castling.x=0;if (p.x==7&&((king*)M[y][4])->castling.y!=0)((king*)M[y][4])->castling.y=0;return true;}
    return false;

}


         bishop::bishop (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){if (col)piece::img=new MeshObj("Chess/bb.obj");else piece::img=new MeshObj("Chess/bn.obj");};
void bishop::allowedxy(bool free,piece* M [8] [8])
    {   allowed.clear();
        pos p1;
       bool T[4]={false,false,false,false};



piece::allowedxy(free,M);
if (!blocked)
{for (int i=1;i<8&&( ((p.y+i<8)&&(p.x+i<8))||((p.y-i>=0)&&(p.x+i<8))||((p.y-i>=0)&&(p.x-i>=0))||((p.y+i<8)&&(p.x-i>=0)));i++)

   {

     if(!T[0]&&p.y+i<8&&p.x+i<8)
    {
        if(M[p.y+i][p.x+i]==NULL||(free&&M[p.y+i][p.x+i]->col!=col))
        {p1.y=p.y+i;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+i][p.x+i]->col!=col)
                {p1.y=p.y+i;p1.x=p.x+i;allowed.push_back(p1);}
            T[0]=true;

        }
    }
    if(!T[1]&&p.y-i>=0&&p.x-i>=0)
    {
        if(M[p.y-i][p.x-i]==NULL||(free&&M[p.y-i][p.x+i]->col!=col))
        {p1.y=p.y-i;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-i][p.x-i]->col!=col)
                {p1.y=p.y-i;p1.x=p.x-i;allowed.push_back(p1);}
            T[1]=true;

        }
    }
      if(!T[2]&&p.y+i<8&&p.x-i>=0)
    {
        if(M[p.y+i][p.x-i]==NULL||(free&&M[p.y+i][p.x-i]->col!=col))
        {p1.y=p.y+i;p1.x=p.x-i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+i][p.x-i]->col!=col)
                {p1.y=p.y+i;p1.x=p.x-i;allowed.push_back(p1);}
            T[2]=true;

        }
    }
      if(!T[3]&&p.y-i>=0&&p.x+i<8)
    {
        if(M[p.y-i][p.x+i]==NULL||(free&&M[p.y-i][p.x+i]->col!=col))
        {p1.y=p.y-i;p1.x=p.x+i;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-i][p.x+i]->col!=col)
                {p1.y=p.y-i;p1.x=p.x+i;allowed.push_back(p1);}
            T[3]=true;

        }
    }
   }
}


;}


         horse::horse (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){if (col)piece::img=new MeshObj("Chess/hb.obj");else piece::img=new MeshObj("Chess/hn.obj");};
void horse::allowedxy(bool free,piece* M [8] [8])

{allowed.clear();
        pos p1;


piece::allowedxy(free,M);

 if (!blocked)
   {

   if(p.y+2<8&&p.x+1<8)
    {
        if(M[p.y+2][p.x+1]==NULL||free)
        {p1.y=p.y+2;p1.x=p.x+1;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+2][p.x+1]->col!=col)
               {p1.y=p.y+2;p1.x=p.x+1;allowed.push_back(p1);}


        }
    }

     if(p.y+1<8&&p.x+2<8)
    {
        if(M[p.y+1][p.x+2]==NULL||free)
        {p1.y=p.y+1;p1.x=p.x+2;allowed.push_back(p1);}
        else
        {
           // if (M[p.y+1][p.x+2]->col!=col)
               {p1.y=p.y+1;p1.x=p.x+2;allowed.push_back(p1);}


        }
    }
     if(p.y+2<8&&p.x-1>=0)
    {
        if(M[p.y+2][p.x-1]==NULL||free)
        {p1.y=p.y+2;p1.x=p.x-1;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+2][p.x-1]->col!=col)
               {p1.y=p.y+2;p1.x=p.x-1;allowed.push_back(p1);}


        }
    }
        if(p.y+1<8&&p.x-2>=0)
    {
        if(M[p.y+1][p.x-2]==NULL||free)
        {p1.y=p.y+1;p1.x=p.x-2;allowed.push_back(p1);}
        else
        {
            //if (M[p.y+1][p.x-2]->col!=col)
               {p1.y=p.y+1;p1.x=p.x-2;allowed.push_back(p1);}


        }
    }

        if(p.y-2>=0&&p.x+1<8)
    {
        if(M[p.y-2][p.x+1]==NULL||free)
        {p1.y=p.y-2;p1.x=p.x+1;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-2][p.x+1]->col!=col)
               {p1.y=p.y-2;p1.x=p.x+1;allowed.push_back(p1);}


        }
    }
         if(p.y-2>=0&&p.x-1>=0)
    {
        if(M[p.y-2][p.x-1]==NULL||free)
        {p1.y=p.y-2;p1.x=p.x-1;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-2][p.x-1]->col!=col)
               {p1.y=p.y-2;p1.x=p.x-1;allowed.push_back(p1);}


        }
    }
        if(p.y-1>=0&&p.x-2>=0)
    {
        if(M[p.y-1][p.x-2]==NULL||free)
        {p1.y=p.y-1;p1.x=p.x-2;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-1][p.x-2]->col!=col)
               {p1.y=p.y-1;p1.x=p.x-2;allowed.push_back(p1);}


        }
    }
        if(p.y-1>=0&&p.x+2<8)
    {
        if(M[p.y-1][p.x+2]==NULL||free)
        {p1.y=p.y-1;p1.x=p.x+2;allowed.push_back(p1);}
        else
        {
            //if (M[p.y-1][p.x+2]->col!=col)
               {p1.y=p.y-1;p1.x=p.x+2;allowed.push_back(p1);}


        }
    }


   }


;}




 pion::pion (int m,int n,bool c,MeshObj* i):piece (m,n,c,i){};

void pion::allowedxy(bool free,piece* M [8] [8])
{
allowed.clear();
pos p1;

piece::allowedxy(free,M);

if (col&&!blocked)
{






    if (p.y<7)
    {

    if (M[p.y+1][p.x]==NULL||free)
{p1.x=p.x;p1.y=p.y+1;allowed.push_back(p1);}
if (p.y==1&&M[p.y+1][p.x]==NULL&&M[p.y+2][p.x]==NULL||free)
  {p1.x=p.x;p1.y=p.y+2;allowed.push_back(p1);}
if(p.x<7&&p.x==flag.x-1&&p.y==flag.y-1&&M[p.y][p.x+1]!=NULL)
{p1.x=p.x+1;p1.y=p.y+1;allowed.push_back(p1);}
if(p.x>1&&p.x==flag.x+1&&p.y==flag.y-1&&M[p.y][p.x-1]!=NULL)
{p1.x=p.x-1;p1.y=p.y+1;allowed.push_back(p1);}

if (p.x<7&&M[p.y+1][p.x+1]!=NULL)//&&M[p.y+1][p.x+1]->col==false)
{p1.x=p.x+1;p1.y=p.y+1;allowed.push_back(p1);}
if (p.x>1&&M[p.y+1][p.x-1]!=NULL)//&&M[p.y+1][p.x-1]->col==false)
{p1.x=p.x-1;p1.y=p.y+1;allowed.push_back(p1);}

}

}

else {

if (p.y>1&&!blocked)
{


        if (M[p.y-1][p.x]==NULL||free)
{p1.x=p.x;p1.y=p.y-1;allowed.push_back(p1);
if (p.y==6&&M[p.y-1][p.x]==NULL&&M[p.y-2][p.x]==NULL||free)
  {p1.x=p.x;p1.y=p.y-2;allowed.push_back(p1);}
  }
if(p.x<7&&p.x==flag.x-1&&p.y==flag.y+1&&M[p.y][p.x+1]!=NULL)
{p1.x=p.x+1;p1.y=p.y-1;allowed.push_back(p1);}
if(p.x>1&&p.x==flag.x+1&&p.y==flag.y+1&&M[p.y][p.x-1]!=NULL)
{p1.x=p.x-1;p1.y=p.y-1;allowed.push_back(p1);}


if (p.x<7&&M[p.y-1][p.x+1]!=NULL)//&&M[p.y-1][p.x+1]->col==true)
{p1.x=p.x+1;p1.y=p.y-1;allowed.push_back(p1);}
if (p.x>1&&M[p.y-1][p.x-1]!=NULL)//&&M[p.y-1][p.x-1]->col==true)
{p1.x=p.x-1;p1.y=p.y-1;allowed.push_back(p1);}

}
}
;}

bool checkdir(pos p1,pos p2,pos p3)
{
    short x,y,x1,y1;
    x=p3.x-p1.x;
    y=p3.y-p1.y;
    x1=p3.x-p2.x;
    y1=p3.y-p2.y;
    if (y1==0&&x1==0)
        return true;
    if ((abs(x)==abs(y)||x==0||y==0)&&(abs(x1)==abs(y1)||x1==0||y1==0))
        {if (x!=0)x/=abs(x);
    if (y!=0)y/=abs(y);
      if (x1!=0)x1/=abs(x1);
    if (y1!=0)y1/=abs(y1);

    if (x==x1&&y==y1)
        return true;

    }
    return false ;
}

