#include <windows.h>
#include <SDL/SDL.h>

#include <GL/gl.h>
#include <iostream>
#include <GL/glu.h>
#include "sdlglutils.h"
#include <cstdlib>
#include "OBJlib.h"
#include <vector>
#define W 800
#define H 600
#define FPS 30

using namespace std;
class TrackBallCamera

{
protected:

    double _motionSensivity;

    double _scrollSensivity;

    bool _hold;

    double _distance;

    double _angleZ;

    double _angleY;

    SDL_Cursor * _hand1;

    SDL_Cursor * _hand2;
public:
 TrackBallCamera();
 virtual void OnMouseMotion(const SDL_MouseMotionEvent & event);
 virtual void OnMouseButton(const SDL_MouseButtonEvent & event);
 virtual void OnKeyboard(const SDL_KeyboardEvent & event);
 virtual void look();
 virtual ~TrackBallCamera();};

  bool getmn (Uint16 x,Uint16 y,int* m,int* n);

