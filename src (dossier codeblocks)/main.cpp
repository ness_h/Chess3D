#include <windows.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <cstdlib>
#include "sdlglutils.h"
#include "OBJlib.h"
#include "3dpart.h"
#include "piece.h"
#include <stack>
#include <ft2build.h>
#include <typeinfo>
#include "freetype.h"

#define W 800
#define H 600
#define FPS 30

using namespace std;
int ct=0,lt=0;
freetype_mod::font_data our_font;
freetype_mod::font_data uri_font;
freetype_mod::font_data our_font1;
freetype_mod::font_data our_font2;
GLuint texture1;
TrackBallCamera * camera;


MeshObj *pionB=new MeshObj("Chess/pawn.obj");
MeshObj *pionN=new MeshObj("Chess/bpawn.obj");

struct mvt
{
    pos ap;
   piece* pi;
};

class Player
{


public :
    static stack<mvt> rfuture;
    static piece* board[8][8];
    static int turn ;
    static stack <mvt> past;
    static pos score;
    piece *p;
    static vector <piece*> Eaten;
    bool col;

Player(bool c):col(c){score.x=0;score.y=0;};
void promote (piece* P)
    {
        if (P->p.y==7*col&&(P->img==pionB||P->img==pionN)){int m=P->p.x,n=P->p.y;bool c=P->col;delete P;P=new queen (m,n,c);P->select(board);};
    }
bool play (SDL_Event event)
    {
 int m,n;
bool checkmate = false;
mvt mov;
        if (event.button.x>W*16/20&&event.button.x<W*17/20&&event.button.y<H-30&&event.button.y>H-55)
                    {
                        if(!past.empty())
                        {mov.pi=past.top().pi;
                        mov.ap.x=mov.pi->p.x;
                        mov.ap.y=mov.pi->p.y;

                            past.top().pi->moveto(past.top().ap.x,past.top().ap.y,board);
                             if (!Eaten.empty()&&((Eaten[Eaten.size()-1]->p.x==mov.ap.x&&Eaten[Eaten.size()-1]->p.y==mov.ap.y)||(typeid((*(mov.pi))).name()==typeid(pion).name()&&
                                                                                                                                Eaten[Eaten.size()-1]->p.x
                                                                                                                                ==mov.ap.x&&
                                                                                                                                Eaten[Eaten.size()-1]->p.y==mov.ap.y+(1-2*mov.pi->col))))
                        {
                            Eaten[Eaten.size()-1]->moveto(Eaten[Eaten.size()-1]->p.x,Eaten[Eaten.size()-1]->p.y,board);
                                if (!col)
                                  score.x-=  Eaten[Eaten.size()-1]->getu();
                                  else
                                    score.y-=  Eaten[Eaten.size()-1]->getu();
                            Eaten.pop_back();

                        }
                        rfuture.push(mov);
                        past.pop();
                        turn--;}
                    }

        else if (event.button.x>W*18/20&&event.button.x<W*19/20&&event.button.y<H-30&&event.button.y>H-55)
        {if(!rfuture.empty())
                        {
                            mov.pi=rfuture.top().pi;
                        mov.ap.x=mov.pi->p.x;
                        mov.ap.y=mov.pi->p.y;
if (board[rfuture.top().ap.y][rfuture.top().ap.x]!=NULL)
                            {Eaten.push_back(board[rfuture.top().ap.y][rfuture.top().ap.x]);
if (col)
                                  score.x+=  Eaten[Eaten.size()-1]->getu();
                                  else
                                    score.y+=  Eaten[Eaten.size()-1]->getu();}
                            else if (typeid((*(mov.pi))).name()==typeid(pion).name()&&
                                     board[rfuture.top().ap.y+(1-2*mov.pi->col)][rfuture.top().ap.x]!=NULL&&board[rfuture.top().ap.y+(1-2*mov.pi->col)][rfuture.top().ap.x]!=mov.pi)
                               {
                                   Eaten.push_back(board[rfuture.top().ap.y+(1-2*mov.pi->col)][rfuture.top().ap.x]);
                                    if (col)
                                  score.x+=  Eaten[Eaten.size()-1]->getu();
                                  else
                                    score.y+=  Eaten[Eaten.size()-1]->getu();}

            rfuture.top().pi->moveto(rfuture.top().ap.x,rfuture.top().ap.y,board);


                        past.push(mov);
                        rfuture.pop();
                        turn++;}
               }

else   if (getmn(event.button.x,event.button.y,&m,&n)) {checkmate = true;
while (!rfuture.empty()) rfuture.pop();
        pos f;
        mvt mov;
        f.x=piece::flag.x;f.y=piece::flag.y;

        piece::Bking->allowedxy(false,board);
        piece::Wking->allowedxy(false,board);
        for (int i=0;i<8;i++)
        for (int j=0;j<8;j++)
        {
        if (board[i][j]!=NULL&&board[i][j]!=piece::Bking&&board[i][j]!=piece::Wking)
        {board[i][j]->allowedxy(false,board);
         board[i][j]->block(&board[i][j]->allowed);
        if (board[i][j]->allowed.size()>0&&board[i][j]->col==col)
        checkmate=false;
        }

}





                         {


                             if(board[n][m]!=NULL&&board[n][m]->col==col)
                             {
                                cout<<(turn%2==1)<<endl;

                                if (p!=NULL)
                                    p->select(board);
                                 p=board[n][m];
                                 p->select(board);



                             }


                             if (p!=NULL)
                             {int x,y;
                                    x=p->p.x;
                                     y=p->p.y;
                                     if (board[n][m]!=NULL&&board[n][m]->col!=col)
                                 { piece* pi;
                                 pi=board[n][m];

                                     if (p->movexy(m,n,board))
                                 { Eaten.push_back(pi);

                                     mov.pi=p;
                                 mov.ap.x=x;mov.ap.y=y;
                                    if (col)
                                    score.x+=pi->getu();
                                    else
                                        score.y+=pi->getu();
                                 past.push(mov);

                                     turn++;promote(p);p->select(board);p=NULL;
                                  if (f.x==piece::flag.x&&f.y==piece::flag.y)
                       {
                           piece::flag.x=9;piece::flag.y=9;
                       }
                                 } }
                                    else if (board[n][m]==NULL){
                                            piece* pi;
                                    pi=board[n-(2*col-1)][m];
                                            if (p->movexy(m,n,board))
                                    {if (typeid((*board[n][m])).name()==typeid(pion).name()&&pi!=NULL&&pi!=p)
                                       {Eaten.push_back(pi);
                                          if (col)
                                    score.x+=pi->getu();
                                    else
                                        score.y+=pi->getu();}
                                        mov.pi=p;
                                 mov.ap.x=x;mov.ap.y=y;
                                        past.push(mov);
                                        turn++;promote(p);p->select(board);p=NULL;
                                     if (f.x==piece::flag.x&&f.y==piece::flag.y)
                       {
                           piece::flag.x=9;piece::flag.y=9;
                       }
                                    }}
                                    }





                         }




}
return checkmate ;
        }






};


Player p2(false),p1(true);


 void Dessiner(TrackBallCamera* camera, piece* M [8][8],bool x)

{
piece* p=NULL;


glClear( GL_COLOR_BUFFER_BIT );

glMatrixMode( GL_MODELVIEW );

    glLoadIdentity( );
//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    camera->look();

glColor3ub(255,255,255);
freetype_mod::print(our_font,W*3/8-30,H*5/6,"Chess 3d ");

if (x==true) freetype_mod::print(our_font1,20,20,"Press Space Button to start game");
else
{
freetype_mod::print(our_font1,20,20,"turn = %d",Player::turn);
 freetype_mod::print(our_font1,20,550,"Score 1= %d",Player::score.x);
  freetype_mod::print(our_font1,620,550,"Score 2= %d",Player::score.y);
  freetype_mod::print(our_font,W*8/10,15,"<-  ->");

}

glEnable(GL_LIGHT0);





glBindTexture(GL_TEXTURE_2D, texture1);
glDisable(GL_LIGHTING);
glBegin(GL_QUADS);




   glColor3ub(255,255,255);


     glTexCoord2d(0,0); glVertex3d(1,0.25,1);
glTexCoord2d(0,1); glVertex3d(1,0.25,-1);

    glTexCoord2d(1,1); glVertex3d(-1,0.25,-1);

     glTexCoord2d(1,0); glVertex3d(-1,0.25,1);
;

    glEnd();
      glBindTexture(GL_TEXTURE_2D, 0);
      glEnable(GL_LIGHTING);
      for (int i=0;i<8;i++)
for (int j=0;j<8;j++)
if (M[i][j]!=NULL)
M[i][j]->draw(M);


    glFlush();

    SDL_GL_SwapBuffers();

}

piece* piece::Bking;
piece* piece::Wking;

    SDL_Event event;

int Player::turn=0;
piece* Player::board[8][8];
pos piece::flag;

stack<mvt> Player::past;
stack<mvt> Player::rfuture;
vector<piece*> Player::Eaten;
pos Player::score;
int main(int argc, char *argv[])

{


    SDL_Init(SDL_INIT_VIDEO);

    atexit(SDL_Quit);

SDL_WM_SetCaption("SDL GL Application", NULL);
SDL_Surface* ecran,*imageDeFond;
ecran=SDL_SetVideoMode(W, H, 32, SDL_OPENGL);
    
	glEnable(GL_DEPTH_TEST);

    glMatrixMode( GL_PROJECTION );

    glLoadIdentity();

    gluPerspective(70,(double)W/H,1,1000);

piece::flag.x=9;piece::flag.y=9;

for (int i=0;i<8;i++)
for (int j=0;j<8;j++)
Player::board[i][j]=NULL;

glEnable(GL_DEPTH_TEST);
glEnable(GL_TEXTURE_2D);

  
texture1 = loadTexture("board.bmp");
GLfloat light_position[] = { 0., 1.0, 0., 0 };

glClearColor (0.1, 0.1, 0.1, 0.1);
glMateriali(GL_FRONT, GL_SHININESS, 7);
float specReflection[] = { 0.01f, 0.01f, 0.01f, 1.0f };

glMaterialfv(GL_FRONT, GL_SPECULAR, specReflection);
glShadeModel (GL_SMOOTH);
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
glLightfv(GL_LIGHT0, GL_POSITION, light_position);

   
our_font.init("SIXTY.TTF", W/40);
our_font1.init("SIXTY.TTF",10);

 

     for (short i=0;i<8;i++)
        {
            Player::board[6][i]=new pion(i,6,false,pionN);  Player::board[6][i]->setu(1);
            Player::board[1][i]=new pion(i,1,true,pionB);Player::board[1][i]->setu(1);


            }
            Player::board[7][4]=new king(4,7,false);piece::Bking=Player::board[7][4];
Player::board[0][4]=new king(4,0,true);;piece::Wking=Player::board[0][4];
Player::board[7][3]=new queen(3,7,false);Player::board[7][3]->setu(8);
Player::board[0][3]=new queen(3,0,true);Player::board[0][3]->setu(8);
Player::board[7][0]=new rook(0,7,false);Player::board[7][0]->setu(4);
Player::board[0][0]=new rook(0,0,true);Player::board[0][0]->setu(4);
Player::board[7][7]=new rook(7,7,false);Player::board[7][7]->setu(4);
Player::board[0][7]=new rook(7,0,true);Player::board[0][7]->setu(4);
Player::board[7][2]=new bishop(2,7,false);Player::board[7][2]->setu(3);
Player::board[0][2]=new bishop(2,0,true);Player::board[0][2]->setu(3);
Player::board[7][5]=new bishop(5,7,false);Player::board[7][5]->setu(3);
Player::board[0][5]=new bishop(5,0,true);Player::board[0][5]->setu(3);
Player::board[7][1]=new horse(1,7,false);Player::board[7][1]->setu(3);
Player::board[0][1]=new horse(1,0,true);Player::board[0][1]->setu(3);
Player::board[7][6]=new horse(6,7,false);Player::board[7][6]->setu(3);
Player::board[0][6]=new horse(6,0,true);Player::board[0][6]->setu(3);
for (int i=0;i<8;i++)
  for (int j=0;j<8;j++)
  if (Player::board[i][j]!=NULL)
   Player::board[i][j]->allowedxy(false,Player::board);


bool jeu=true;
bool test=true;
glEnable(GL_LIGHTING);
glEnable(GL_LIGHT0);
glEnable(GL_COLOR_MATERIAL);
camera = new TrackBallCamera();

   


   while (test)

    {

  ct =SDL_GetTicks();

Dessiner(camera,Player::board,jeu);


        while (SDL_PollEvent(&event))

        {


            switch(event.type)

            {

                case SDL_QUIT:

                exit(0);

                break;
                case SDL_KEYDOWN:

                switch (event.key.keysym.sym)
                {

                case SDLK_SPACE:
                    jeu=false;break;
                    case SDLK_p:
                    takeScreenshot("test.bmp");
                    break;
                    case SDLK_ESCAPE:
                    exit(0);
                    break;
                    default : //on a utilis� la touche P et la touche ECHAP, le reste est donn� � la cam�ra
                    camera->OnKeyboard(event.key);
                }

                break;
                case SDL_MOUSEMOTION:
                    //la souris est boug�e, �a n'int�resse que la cam�ra
                camera->OnMouseMotion(event.motion);
                break;
                case SDL_MOUSEBUTTONUP:
                    camera->OnMouseButton(event.button);
                break;

                case SDL_MOUSEBUTTONDOWN: if(jeu==false)
                {




if (Player::turn%2==0)
{if (p1.play(event)){cout<<"joueur 1 echec et mat"<<endl;}}else if(p2.play(event)){cout<<"joueur 2 echec et mat"<<endl;}

                }

                    camera->OnMouseButton(event.button);
                break;

            }

        }



glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) ;
//cout << SDL_GetTicks()-ct<<endl;
    if (SDL_GetTicks()-ct<1000/FPS)
    SDL_Delay(1000/FPS-(SDL_GetTicks()-ct));
    }

 SDL_FreeSurface(imageDeFond);
    return 0;

}



