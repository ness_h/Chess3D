

#include "OBJlib.h"

using namespace std;

struct pos
{
short x,y;
};

class piece
{
protected :



public :
    static piece* Bking;
    static piece* Wking;
    static pos flag;
    bool blocked;
vector <piece*>check;
    bool col ;
int u;
    vector <pos> allowed;
pos p;

MeshObj* img  ;
bool s;
 piece (short m,short n,bool c,MeshObj* i);
 void add (short m=1,short n=1);
 short getx();
short gety();
void setu(int y){u=y;}
int getu(){return u;}
pos block (vector <pos>* V);
void setxy (short m,short n);
virtual void allowedxy(bool free,piece* M [8] [8]) ;
void select (piece* M[8][8]);
virtual void moveto (int m,short n,piece* M [8] [8]);
virtual bool movexy(short m,short n,piece* M [8] [8]);
void draw(piece* M[8][8]);
};
class queen : public piece
{public :

     queen (int m=2,int n=2,bool c=false,MeshObj* i=NULL);

    void allowedxy(bool free,piece* M [8] [8]);

};
class king : public piece
{

    public :
        vector <pos> prohibited;
       pos castling;
        king (int m=2,int n=2,bool c=false,MeshObj* i=NULL);
void prohibitedxy (piece* M [8] [8]);
void allowedxy(bool free,piece* M [8] [8]);
bool movexy (short  m,short n,piece* M[8][8]);
};
class rook:public piece
{
public :
         rook (int m=2,int n=2,bool c=false,MeshObj* i=NULL);
         void allowedxy(bool free,piece* M [8] [8]);
         bool movexy (short m,short n,piece* M[8][8]);
};
class bishop : public piece

{
    public :
        bishop (int m=2,int n=2,bool c=false,MeshObj* i=NULL);
        void allowedxy(bool free,piece* M [8] [8]);
};
class horse : public piece
{

    public :
        horse (int m=2,int n=2,bool c=false,MeshObj* i=NULL);
        void allowedxy(bool free,piece* M [8] [8]);
};
class pion : public piece
{
private:

public :
    pion (int m=2,int n=2,bool c=false,MeshObj* i=NULL);
        void allowedxy(bool free,piece* M [8] [8]);
};

bool checkdir (pos,pos,pos);